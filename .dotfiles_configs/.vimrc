set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}


""""Git Plugins""""
"Git commands in vim
Plugin 'tpope/vim-fugitive'
"Git markings
Plugin 'airblade/vim-gitgutter'

""""Appearance/QOL Plugins""""
"Status bar
Plugin 'itchyny/lightline.vim'      
"Indent markings
Plugin 'Yggdroot/indentLine'
"Parantheses/XML tags/other - cs<foo><bar>
Plugin 'tpope/vim-surround'
Plugin 'frazrepo/vim-rainbow'
"Multiline editing
Plugin 'mg979/vim-visual-multi'
"Comment out sections
Plugin 'tpope/vim-commentary'
""""File system Plugins""""
"Directory browser
Plugin 'preservim/nerdtree'
"Fuzzy search
Plugin 'junegunn/fzf.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin Configs
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set shell=/bin/bash
"Nerdtree
map <F5> :NERDTreeToggle<CR>
let NERDTreeShowHidden=1

"fzf search
map <F4> :Files<CR>
let g:fzf_layout = { 'up': '50%' }

let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }


"Git Gutter
set updatetime=100

"Rainbow Parantheses
let g:rainbow_active = 0

"Indent Tabs
let g:indentLine_enabled = 1
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

"Lightline
set laststatus=2
set noshowmode

let g:lightline = {
      \ 'colorscheme': 'seoul256',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \ 'gitbranch': 'FugitiveHead'
      \ },
      \ }


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General vim configs
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"Color Scheme
colorscheme iceberg
"colorscheme nord
"Syntax highlighting
syntax on

"Indentation
set expandtab
set autoindent
set smartindent
set tabstop=4
"Default to shift width
set softtabstop=-1
"Default to tabstop
set shiftwidth=0

"Line numbers
set number
set relativenumber
set ruler
set cursorline
set cursorcolumn

"Ability to use mouse in all modes
set mouse=a

 "Proper backspace behavior.
set backspace=indent,eol,start

" Great command-line completion, use `<Tab>` to move
" around and `<CR>` to validate.
set wildmenu

"Show tabs
set list
set listchars=tab:>-

"Search Options
"set hlsearch

"Code folding
"set foldmethod=indent
