#!/usr/bin/env sh

# Kill polybar
killall -q polybar

# Make sure processes are killed
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch the bars
polybar TopBar -r &
polybar BottomBar -r
