local map = vim.api.nvim_set_keymap
-- map the leader key
map('n', '\\', '', {})
vim.g.mapleader = '\\'  -- 'vim.g' sets global variables

options = { noremap = true }
map('n', '<C-s>', ':w<cr>', options)
map('n', '<C-e>', ':E<cr>', options)
--map('n', '<leader>s', ':w<cr>', options)

      

-- ==============================
--       Plugin keybinds
-- ==============================
-- nvim-tree file explorer
map('n', '<leader>e', ':NvimTreeToggle<CR>', options)
map('n', '<leader>r', ':NvimTreeRefresh<CR>', options)
map('n', '<leader>n', ':NvimTreeFindFile<CR>', options)

-- vbox ascii diagram drawing
map('v', '<leader>d', ':VBoxO<CR>', options)
map('v', '<leader><S-d>', ':VBoxHO<CR>', options)
map('v', '<leader>f', ':VBoxDO<CR>', options)

-- nvim-term
map('n', '<leader>t', '<Esc><Cmd>exe v:count1 . "ToggleTerm"<CR>', options)
map('i', '<leader>t', '<Esc><Cmd>exe v:count1 . "ToggleTerm"<CR>', options)
map('t', '<leader><C-t>', '<Esc><Cmd>exe v:count1 . "ToggleTerm"<CR>', options)

-- git-blame
map('n', '<leader>gb', ':GitBlameToggle<CR>', options)
