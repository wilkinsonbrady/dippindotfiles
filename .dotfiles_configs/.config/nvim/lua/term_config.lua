local opt = vim.opt

--allows cursor to be placed even when theres not a char
--opt.ve= "all"

opt.termguicolors = true
opt.showmode = false

--Indentation
opt.expandtab = true
opt.autoindent = true
opt.smartindent = true
opt.tabstop=4
--Default to shift width
opt.softtabstop=-1
--Default to tabstop
opt.shiftwidth=0

--Line numbers
opt.number = true
opt.relativenumber = true
opt.ruler = true
opt.cursorline = true
opt.cursorcolumn = true

-- Ability to use mouse in all modes
opt.mouse="a"

-- Proper backspace behavior.
opt.backspace="indent,eol,start"

-- Great command-line completion, use `<Tab>` to move
-- around and `<CR>` to validate.
opt.wildmenu = true

-- Show tabs
opt.list = true
opt.listchars="tab:>-"

