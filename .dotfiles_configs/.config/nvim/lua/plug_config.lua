--Plugin Manager
require "paq" {
    --Prerequisites
    "savq/paq-nvim";                  -- Let Paq manage itself
    "neovim/nvim-lspconfig";          -- Mind the semi-colons
    'kyazdani42/nvim-web-devicons';
    'nvim-lua/plenary.nvim';
    'nvim-treesitter/nvim-treesitter';
    {"lervag/vimtex", opt=true};      -- Use braces when passing options

    --Themes
    "rktjmp/lush.nvim";
    'Shadorain/shadotheme';
    'folke/tokyonight.nvim';
    {"npxbr/gruvbox.nvim", requires = {"rktjmp/lush.nvim"}};
    'sainnhe/everforest';

    --Autocomplete                                       
    "hrsh7th/nvim-compe";                                

    --Statusbar                                          
    'famiu/feline.nvim';                                 

    --Indents
    "lukas-reineke/indent-blankline.nvim";

    --File Editing
    -- "p00f/nvim-ts-rainbow";

    --File Browsing
    'akinsho/nvim-bufferline.lua';
    'kyazdani42/nvim-tree.lua';
    'dstein64/nvim-scrollview';
    'karb94/neoscroll.nvim';

    --Commenting
    'b3nj5m1n/kommentary';
    'jbyuki/venn.nvim';

    --Terminal Integration
    "akinsho/nvim-toggleterm.lua";

    --Git
    -- 'tanvirtin/vgit.nvim';
    'lewis6991/gitsigns.nvim';
    'f-person/git-blame.nvim';
}
-- ==============================
--        Color Scheme
-- ==============================
vim.o.background = "dark" -- or "light" for light mode
-- vim.g.everforest_background='hard'
-- vim.cmd([[colorscheme everforest]])
vim.g.tokyonight_style = "storm"
vim.cmd([[colorscheme tokyonight]])
-- vim.cmd([[colorscheme gruvbox]])
-- vim.cmd([[colorscheme xshado]])


-- ==============================
--    Statusbar Configurations
-- ==============================
require('feline').setup({
    preset = 'noicon'
})


-- ==============================
--       Indents Config
-- ==============================
require("indent_blankline").setup {
    char = "|",
    buftype_exclude = {"terminal"}
}

-- ==============================
--         File Editing
-- ==============================




-- ==============================
--      File Explorer Configs
-- ==============================
vim.g.nvim_tree_disable_netrw = 0
vim.g.nvim_tree_hijack_netrw = 0
require('neoscroll').setup()
require("bufferline").setup{
    options = {
        numbers = "ordinal",
        offsets = {{filetype = "NvimTree", text = "File Explorer"}},
        separator_style = "padded_slant"
    }
}


-- ==============================
--          Git Configs
-- ==============================
require('gitsigns').setup()
-- require('vgit').setup()

-- ==============================
--      Terminal Configs
-- ==============================
require("toggleterm").setup{}  
  
  
  
  
-- ==============================
--   Autocomplete Config - WIP
-- ==============================
-- vim.o.completeopt = "menuone,noselect"
-- require'compe'.setup {
--   enabled = true;
--   autocomplete = true;
--   debug = false;
--   min_length = 1;
--   preselect = 'enable';
--   throttle_time = 80;
--   source_timeout = 200;
--   resolve_timeout = 800;
--   incomplete_delay = 400;
--   max_abbr_width = 100;
--   max_kind_width = 100;
--   max_menu_width = 100;
--   documentation = {
--     border = { '', '' ,'', ' ', '', '', '', ' ' }, -- the border option is the same as `|help nvim_open_win|`
--     winhighlight = "NormalFloat:CompeDocumentation,FloatBorder:CompeDocumentationBorder",
--     max_width = 120,
--     min_width = 60,
--     max_height = math.floor(vim.o.lines * 0.3),
--     min_height = 1,
--   };
-- 
--   source = {
--     path = true;
--     buffer = true;
--     calc = true;
--     nvim_lsp = true;
--     nvim_lua = true;
--     vsnip = true;
--     ultisnips = true;
--     luasnip = true;
--   };
-- }
-- vim.cmd([[
-- inoremap <silent><expr> <C-Space> compe#complete()
-- inoremap <silent><expr> <CR>      compe#confirm('<CR>')
-- inoremap <silent><expr> <C-e>     compe#close('<C-e>')
-- inoremap <silent><expr> <C-f>     compe#scroll({ 'delta': +4 })
-- inoremap <silent><expr> <C-d>     compe#scroll({ 'delta': -4 })
-- ]])
--
